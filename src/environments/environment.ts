// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// https://nearable.app/
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDMant2Jv9k6fZJmmasGKZjtyZvVYq_abQ",
    authDomain: "nearable-pwa.firebaseapp.com",
    databaseURL: "https://nearable-pwa.firebaseio.com",
    projectId: "nearable-pwa",
    storageBucket: "nearable-pwa.appspot.com",
    messagingSenderId: "947242011312",
    appId: "1:947242011312:web:95feaa98cce0b2ad7f4975",
    measurementId: "G-9G12YDT4QK"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
