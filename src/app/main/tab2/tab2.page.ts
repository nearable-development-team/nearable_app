import { Component } from '@angular/core';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { ActivatedRoute } from "@angular/router";
import { FolderService } from 'src/app/services/folder/folder.service';
import { Photo, Folder } from 'src/app/interface/interface';
import { AuthService } from 'src/app/services/auth/auth.service';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { ModalController } from '@ionic/angular';
import { ViewerModalComponent } from 'ngx-ionic-image-viewer';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
    public IsWebRTCSupported: boolean;
    constructor(
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public photoService: PhotoService,
        private toastService: ToastService,
        public folderService: FolderService,
        public authService: AuthService,
        private route: ActivatedRoute,
        private webRtcService: WebRtcService,
        private modalController: ModalController,
        private navController: NavController,


    ) {
        this.route.queryParams.subscribe(params => {
            if (params["folder"]) {
                this.CurrentFolder = JSON.parse(params["folder"]) as Folder;
                this.currentFolderName = this.CurrentFolder.Name;
                this.currentFolderId = this.CurrentFolder.Id;
                this.folderAuthorId = this.CurrentFolder.AuthorId
            }
        });
    }
    private CurrentFolder: Folder = null;
    public currentFolderName: string = "";
    public currentFolderId: number = 0;
    public folderAuthorId: string;

    public IsRegisteredUser;
    public Photos: Photo[] = [];
    public BaseImagePath;

    async ionViewWillEnter() {
        this.webRtcService.init('tab-video');
        this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
        let userInfo = await this.authService.GetUserInfoLocal();
        this.IsRegisteredUser = userInfo.login;

        if (!this.IsRegisteredUser)
            this.currentFolderName = userInfo.Name;

        this.loadImageByFolder();
    }
    async ngOnInit() {
    }

    private loadImagesFromResponse(response) {
        this.Photos = response.Images;
        this.BaseImagePath = response.BasePath;
    }
    async loadImageByFolder() {
        const loading = await this.loadingCtrl.create({
            message: 'Please wait...',
        });
        loading.present();
        let userInfo = await this.authService.GetUserInfoLocal();

        if (this.IsRegisteredUser)
            this.loadImagesFromResponse(
                await this.photoService.
                    loadImageFromServer(this.currentFolderName, this.currentFolderId, this.folderAuthorId));
        else
            this.loadImagesFromResponse(await this.photoService.loadImageByUniqueSharedId(userInfo.SharedFolderId));

        loading.dismiss();
    }

    async folderNameSelectionChange(ref: string) {
        const loading = await this.loadingCtrl.create({
            message: 'Please wait...',
        });
        loading.present();
        console.log(ref);
        this.currentFolderName = ref;
        await this.loadImageByFolder();
        loading.dismiss();
    }
    async viewImage(imgPath) {
        console.log(imgPath);
        const modal = await this.modalController.create({
            component: ViewerModalComponent,
            componentProps: {
                src: imgPath
            },
            cssClass: 'ion-img-viewer',
            keyboardClose: false,
            showBackdrop: true
        });

        return await modal.present();
    }

    async loadFile(event) {
        let ref = this;
        let base64 = await this.photoService.convertBlobToBase64(event.target.files[0])

        if (this.IsRegisteredUser) {

            let selectedFolders = [this.CurrentFolder];
            if (selectedFolders.length > 0) {
                await ref.photoService.SaveBaseImage(selectedFolders, "", base64);
                ref.toastService.presentToast("uploaded", 500)
            } else {
                ref.toastService.presentToast("upload canceled", 500)
            }

        } else {
            let userInfo = await this.authService.GetUserInfoLocal();
            await ref.photoService.SaveBaseImage(userInfo.Name, userInfo.SharedFolderId, base64);
            ref.toastService.presentToast("uploaded", 500)
            ref.photoService.loadImageByUniqueSharedId(userInfo.SharedFolderId);
        }
        this.loadImageByFolder();
    }

    async goToHome() {
        this.navController.navigateForward('main/camera-screen-new')

    }
}
