import { Component, OnInit } from '@angular/core';
import { StripeService } from 'src/app/services/stripe/stripe.service';
import { CheckoutPageViewModal } from 'src/app/ViewModal/CardInformation';
import { ModalService } from 'src/app/services/modal/modal.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { AuthService } from 'src/app/services/auth/auth.service';
declare var Stripe;
@Component({
  selector: 'app-checkoutpage',
  templateUrl: './checkoutpage.page.html',
  styleUrls: ['./checkoutpage.page.scss'],
})
export class CheckoutpagePage implements OnInit {
  public ViewModal: CheckoutPageViewModal;
  public ServiceCharge: number = 5;
  public PlaceInfo: string;
  public isCustomAmountHidden: boolean = true;
  public PlaceId: string;
  public PlaceName: string;
  public PlaceAddress: string;
  public Password: string;
  public Latitude: string;
  public Longitude: string;

  ionicForm: FormGroup;
  isSubmitted = false;
  constructor(private stripeService: StripeService,
    private modalService: ModalService,
    public formBuilder: FormBuilder,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private authService: AuthService) {
    this.ViewModal = new CheckoutPageViewModal();
    this.ViewModal.Amount = 10;
  }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      CardHolderName: ['', [Validators.required, Validators.minLength(3)]],
      Amount: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      CardHolderEmail: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')]],
      PersonalMessage: ['', [Validators.required]],
      CardNumber: ['', [Validators.required, Validators.maxLength(16), Validators.minLength(16), Validators.pattern('^[0-9]+$')]],
      ExpiryMonth: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
      ExpiryYear: ['', [Validators.required]],
      CCV: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[0-9]+$')]],
      RecFirstName: ['', [Validators.required, Validators.minLength(2)]],
      RecLastName: ['', [Validators.required, Validators.minLength(2)]],
      RecAddress: ['', [Validators.required, Validators.minLength(6)]],
      RecCountry: ['', [Validators.required, Validators.minLength(2)]],
      RecCity: ['', [Validators.required, Validators.minLength(3)]],
      RecState: ['', [Validators.required, Validators.minLength(3)]],
      RecPostalCode: ['', [Validators.required, Validators.pattern('^[0-9]+$'), Validators.minLength(3)]],
    })
  }

  async segmentChanged(e) {
    console.log(e);
    if (e.detail.value == "custom") {
      this.isCustomAmountHidden = false;
    } else {
      this.isCustomAmountHidden = true;
      this.ViewModal.Amount = parseFloat(e.detail.value);
    }

  }
  get getTotalAmount() {
    if (isNaN(parseFloat(this.control.Amount.value))) {
      return this.ServiceCharge
    }
    return this.ServiceCharge + parseFloat(this.control.Amount.value)
  }
  get control() {
    return this.ionicForm.controls;
  }
  async Proceed() {
    var userInfo = await this.authService.GetUserInfoLocal();


    console.log(this.ionicForm);
    let ref = this;
    this.isSubmitted = true;
    if (this.ionicForm.valid) {
      ref.loadingService.presentLoading();
      Stripe.createToken({
        number: this.control.CardNumber.value,
        cvc: this.control.CCV.value,
        exp_month: this.control.ExpiryMonth.value,
        exp_year: this.control.ExpiryYear.value
      }, (status, response) => {

        console.log(status, response);
        if (response.error) {
          ref.toastService.presentToast(response.error.message)
          ref.loadingService.dismiss();
        } else {

          console.log(this.ViewModal);
          this.stripeService.UploadCardDetails({
            Amount: this.control.Amount.value,
            CardHolderEmail: this.control.CardHolderEmail.value,
            CardHolderName: this.control.CardHolderName.value,
            PersonalMessage: this.control.PersonalMessage.value,
            RecLastName: this.control.RecLastName.value,
            RecAddress: this.control.RecAddress.value,
            RecCountry: this.control.RecCountry.value,
            RecCity: this.control.RecCity.value,
            RecState: this.control.RecState.value,
            RecPostalCode: this.control.RecPostalCode.value,
            Token: response['id'],
            Password: this.Password,
            PlaceId: this.PlaceId,
            PlaceName: this.PlaceName,
            UserId: userInfo.Id,
            Latitude: this.Latitude,
            Longitude: this.Latitude

          }).then(() => {
            ref.loadingService.dismiss();
            this.modalService.Dismiss();
          })
        }

      });

    }


  }

}
