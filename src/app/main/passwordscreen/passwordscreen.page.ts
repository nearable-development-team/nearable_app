import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal/modal.service';

@Component({
  selector: 'app-passwordscreen',
  templateUrl: './passwordscreen.page.html',
  styleUrls: ['./passwordscreen.page.scss'],
})
export class PasswordscreenPage implements OnInit {
  public Password: string;
  constructor(private modalService: ModalService) { }

  ngOnInit() {
  }
  async SavePassword() {
    this.modalService.outputSharedProp = {
      Password: this.Password

    };
    this.modalService.Dismiss();
  }
}
