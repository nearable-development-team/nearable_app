import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PasswordscreenPage } from './passwordscreen.page';

describe('PasswordscreenPage', () => {
  let component: PasswordscreenPage;
  let fixture: ComponentFixture<PasswordscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordscreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PasswordscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
