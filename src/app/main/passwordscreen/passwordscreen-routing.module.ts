import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordscreenPage } from './passwordscreen.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordscreenPageRoutingModule {}
