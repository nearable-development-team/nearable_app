import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordscreenPageRoutingModule } from './passwordscreen-routing.module';

import { PasswordscreenPage } from './passwordscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordscreenPageRoutingModule
  ],
  declarations: [PasswordscreenPage]
})
export class PasswordscreenPageModule {}
