import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal/modal.service';

@Component({
  selector: 'app-businessscreen',
  templateUrl: './businessscreen.page.html',
  styleUrls: ['./businessscreen.page.scss'],
})
export class BusinessscreenPage implements OnInit {
  public PlaceId: string;
  public PlaceName: string;
  public PlaceAddress: string;


  constructor(public modalService: ModalService) { }

  ngOnInit() {

  }

  async YesClick() {
    this.modalService.outputSharedProp = {
      PlaceId: this.PlaceId, PlaceName: this.PlaceName, PlaceAddress: this.PlaceAddress,
      isCanceled: false
    };
    this.modalService.Dismiss();
  }

  async cancel() {
    this.modalService.outputSharedProp = { isCanceled: true };
    this.modalService.Dismiss();
  }

}
