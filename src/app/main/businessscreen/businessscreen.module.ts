import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusinessscreenPageRoutingModule } from './businessscreen-routing.module';

import { BusinessscreenPage } from './businessscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessscreenPageRoutingModule
  ],
  declarations: [BusinessscreenPage]
})
export class BusinessscreenPageModule {}
