import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusinessscreenPage } from './businessscreen.page';

describe('BusinessscreenPage', () => {
  let component: BusinessscreenPage;
  let fixture: ComponentFixture<BusinessscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessscreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusinessscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
