import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusinessscreenPage } from './businessscreen.page';

const routes: Routes = [
  {
    path: '',
    component: BusinessscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessscreenPageRoutingModule {}
