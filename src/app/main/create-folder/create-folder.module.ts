import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CreateFolderPageRoutingModule } from './create-folder-routing.module';
import { CreateFolderPage } from './create-folder.page';
import { SharedComponentModule } from 'src/app/component/shared-component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFolderPageRoutingModule,
    SharedComponentModule
  ],
  declarations: [CreateFolderPage]
})
export class CreateFolderPageModule { }
