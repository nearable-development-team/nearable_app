import { Component, OnInit, ViewChild } from '@angular/core';
import { FolderService } from 'src/app/services/folder/folder.service';
import { AlertController, NavController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { GoogleMapComponent } from 'src/app/component/google-map/google-map.component';
import { GeoLocationService } from 'src/app/services/geo-location/GeoLocation.service';
import { UserLocation } from 'src/app/interface/interface';
import { ModalService } from 'src/app/services/modal/modal.service';
import { NearbyIconListPage } from '../nearby-icon-list/nearby-icon-list.page';

@Component({
  selector: 'app-create-folder',
  templateUrl: './create-folder.page.html',
  styleUrls: ['./create-folder.page.scss'],
})
export class CreateFolderPage implements OnInit {
  @ViewChild(GoogleMapComponent, { static: true }) mapComponent: GoogleMapComponent;
  private fenceCenter: UserLocation;
  public FenceAreaInFeet: number;
  public folderName: string;
  public geoFenceEnabled: string;
  public selectedIcon: string;
  public geoFenceWithPrivate: boolean;
  public IsWebRTCSupported: boolean;

  constructor(private folderService: FolderService,
    private alertController: AlertController,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private webRtcService: WebRtcService,
    private navController: NavController,
    private geoLocationService: GeoLocationService,
    private modalService: ModalService,
  ) { }

  async ngOnInit() {

  }
  async ionViewWillEnter() {
    this.webRtcService.init('create-folder-video');
    this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
    this.fenceCenter = await this.geoLocationService.GetLocation();
    this.mapComponent.clearCircle()
  }
  async geoFenceEnabledChanged() {
    this.FenceAreaInFeet = 10;
    this.mapComponent.clearCircle();
  }
  async FenceAreaChanged(FenceAreaInFeet) {
    this.mapComponent.drawCircle(this.fenceCenter, FenceAreaInFeet * 0.3048);
  }

  public async AddNewFolder(folderName: string) {
    console.log(folderName)
    if (folderName) {
      this.loadingService.presentLoading("adding..")
      let rs = await this.folderService
        .AddFolder(folderName,
          this.fenceCenter.Longitude,
          this.fenceCenter.Latitude,
          this.FenceAreaInFeet,
          this.selectedIcon, this.geoFenceWithPrivate);
      console.log(rs);
      await this.folderService.GetAllFolder(true);
      this.toastService.presentToast("added successfuly")
      this.loadingService.dismiss()
      this.folderName = "";
      this.navController.navigateForward('main/create-folder-success', { queryParams: { SharedFolderId: rs.SharedFolderId } });

    } else {
      const alert = await this.alertController.create({
        message: "Please enter folder name",
        buttons: ['OK']
      });
      await alert.present();
    }

  }

  async changeSelectedIcon(icon: string) {
    this.selectedIcon = icon;
  }
  async CancelModal() {
    this.navController.navigateForward('main/camera-screen-new');
  }
  async OpenfolderIconSelectModel() {
    let ref = this;
    await ref.modalService.Present(NearbyIconListPage, async () => {
      let selectedIcon = ref.modalService.outputSharedProp as string;
      this.changeSelectedIcon(selectedIcon);
      console.log(selectedIcon);
    });
  }
}
