import { Component, OnInit } from '@angular/core';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { NavController, AlertController } from '@ionic/angular';
import { FolderService } from 'src/app/services/folder/folder.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-join-nearby',
  templateUrl: './join-nearby.page.html',
  styleUrls: ['./join-nearby.page.scss'],
})
export class JoinNearbyPage implements OnInit {
  public sharedFolderId: string
  IsWebRTCSupported: boolean;
  constructor(private folderService: FolderService,
    private alertController: AlertController,
    private toastService: ToastService,
    private loadingService: LoadingService,
    private webRtcService: WebRtcService,
    private navController: NavController) { }

  async ngOnInit() {
  }
  async ionViewWillEnter() {
    this.webRtcService.init('join-nearby-video');
    this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
  }

  async CancelModal() {
    this.navController.navigateForward('/main/camera-screen-new');
  }

  public async AddSharedFolder(sharedFolderId: string) {
    console.log(sharedFolderId)
    if (sharedFolderId) {
      this.loadingService.presentLoading("adding..")
      await this.folderService.AddSharedFolder(sharedFolderId);
      await this.folderService.GetAllFolder(true);
      this.toastService.presentToast("added successfuly")
      this.loadingService.dismiss()
      this.sharedFolderId = "";
      this.CancelModal()
    } else {
      const alert = await this.alertController.create({
        message: "Please enter folder name",
        buttons: ['OK']
      });
      await alert.present();
    }

  }

}
