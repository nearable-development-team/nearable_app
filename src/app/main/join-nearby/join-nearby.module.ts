import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinNearbyPageRoutingModule } from './join-nearby-routing.module';

import { JoinNearbyPage } from './join-nearby.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JoinNearbyPageRoutingModule
  ],
  declarations: [JoinNearbyPage]
})
export class JoinNearbyPageModule {}
