import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JoinNearbyPage } from './join-nearby.page';

const routes: Routes = [
  {
    path: '',
    component: JoinNearbyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JoinNearbyPageRoutingModule {}
