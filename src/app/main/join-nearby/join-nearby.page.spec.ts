import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoinNearbyPage } from './join-nearby.page';

describe('JoinNearbyPage', () => {
  let component: JoinNearbyPage;
  let fixture: ComponentFixture<JoinNearbyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinNearbyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinNearbyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
