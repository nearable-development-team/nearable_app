import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { CreateFolderSuccessPage } from './create-folder-success.page';


describe('CreateFolderPage', () => {
  let component: CreateFolderSuccessPage;
  let fixture: ComponentFixture<CreateFolderSuccessPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateFolderSuccessPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateFolderSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
