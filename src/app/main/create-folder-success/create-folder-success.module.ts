import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedComponentModule } from 'src/app/component/shared-component.module';
import { CreateFolderSuccessPageRoutingModule } from './create-folder-success-routing.module';
import { CreateFolderSuccessPage } from './create-folder-success.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFolderSuccessPageRoutingModule,
    SharedComponentModule
  ],
  declarations: [CreateFolderSuccessPage]
})
export class CreateFolderSuccessPageModule { }
