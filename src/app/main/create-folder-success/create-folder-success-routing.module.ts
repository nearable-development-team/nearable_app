import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateFolderSuccessPage } from './create-folder-success.page';


const routes: Routes = [
  {
    path: '',
    component: CreateFolderSuccessPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateFolderSuccessPageRoutingModule { }
