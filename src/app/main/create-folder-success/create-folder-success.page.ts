import { Component, OnInit } from '@angular/core';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ShareService } from 'src/app/services/shareService/ShareService.service';

@Component({
  selector: 'app-create-folder-success',
  templateUrl: './create-folder-success.page.html',
  styleUrls: ['./create-folder-success.page.scss'],
})
export class CreateFolderSuccessPage implements OnInit {

  public SharedFolderId: string;
  public share = {
    config: [{
      facebook: {
        socialShareUrl: 'https://peterpeterparker.io'
      }
    }, {
      twitter: {
        socialShareUrl: 'https://peterpeterparker.io'
      }
    }]
  };
  public IsWebRTCSupported: boolean;
  constructor(
    private webRtcService: WebRtcService,
    private route: ActivatedRoute,
    private navController: NavController,
    private shareService: ShareService,
  ) {

    this.route.queryParams.subscribe(params => {
      if (params["SharedFolderId"]) {
        this.SharedFolderId = params["SharedFolderId"];
      }
    });

  }

  async ngOnInit() { }

  async ionViewWillEnter() {
    this.webRtcService.init('create-folder-success');
    this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
  }
  async CancelModal() {
    this.navController.navigateForward('main/camera-screen-new');
  }

  async shareCode(SharedFolderId: string) {
    this.shareService.Share(SharedFolderId);
  }

}
