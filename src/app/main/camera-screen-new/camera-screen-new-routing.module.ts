import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CameraScreenNewPage } from './camera-screen-new.page';

const routes: Routes = [
  {
    path: '',
    component: CameraScreenNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CameraScreenNewPageRoutingModule { }
