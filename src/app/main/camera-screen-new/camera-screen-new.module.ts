import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CameraScreenNewPageRoutingModule } from './camera-screen-new-routing.module';

import { CameraScreenNewPage } from './camera-screen-new.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CameraScreenNewPageRoutingModule
  ],
  declarations: [CameraScreenNewPage]
})
export class CameraScreenNewPageModule { }
