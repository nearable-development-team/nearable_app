import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CameraScreenNewPage } from './camera-screen-new.page';

describe('CameraScreenNewPage', () => {
  let component: CameraScreenNewPage;
  let fixture: ComponentFixture<CameraScreenNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CameraScreenNewPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CameraScreenNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
