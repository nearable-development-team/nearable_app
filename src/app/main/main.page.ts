import { Component } from '@angular/core';
import { FolderService } from '../services/folder/folder.service';


@Component({
  selector: 'app-tabs',
  templateUrl: 'main.page.html',
  styleUrls: ['main.page.scss']
})
export class MainPage {
  constructor(public folderService: FolderService) {

  }
  async ngOnInit() {
    console.log("main pages arrived");
    this.folderService.GetAllFolder(true);
  }
}
