import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CameraScreenPageRoutingModule } from './camera-screen-routing.module';

import { CameraScreenPage } from './camera-screen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CameraScreenPageRoutingModule
  ],
  declarations: [CameraScreenPage]
})
export class CameraScreenPageModule {}
