import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CameraScreenPage } from './camera-screen.page';

describe('CameraScreenPage', () => {
  let component: CameraScreenPage;
  let fixture: ComponentFixture<CameraScreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraScreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CameraScreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
