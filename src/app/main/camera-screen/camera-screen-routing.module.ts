import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CameraScreenPage } from './camera-screen.page';

const routes: Routes = [
  {
    path: '',
    component: CameraScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CameraScreenPageRoutingModule {}
