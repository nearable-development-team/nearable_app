import { Component, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/toast/toast.service';
import { AlertController, NavController } from '@ionic/angular';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { ModalService } from 'src/app/services/modal/modal.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FolderListPage } from '../folder-list/folder-list.page';
import { Folder } from 'src/app/interface/interface';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { CameraView } from 'src/app/interface/enums';
import { Plugins } from '@capacitor/core';
import { FolderService } from 'src/app/services/folder/folder.service';
const { Storage } = Plugins;

@Component({
  selector: 'app-camera-screen',
  templateUrl: './camera-screen.page.html',
  styleUrls: ['./camera-screen.page.scss'],
})
export class CameraScreenPage implements OnInit {
  public camView: CameraView = CameraView.Back;
  public isLightOn: boolean = false;
  public IsRegisteredUser = false;
  public isTorchSupported = false;
  public IsWebRTCSupported = false;
  public classVariable: string = "video-item ";
  public filterFunc = "";
  constructor(private toastService: ToastService,
    private photoService: PhotoService,
    private modalService: ModalService,
    private authService: AuthService,
    private navController: NavController,
    private webRtcService: WebRtcService,
    private folderService: FolderService,
    private alertController: AlertController) { }

  async ngOnInit() {
    console.log(this.IsWebRTCSupported)
  }

  async ionViewWillEnter() {
    this.webRtcService.init('camera-screen-video');
    this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
    let userInfo = await this.authService.GetUserInfoLocal();
    this.IsRegisteredUser = userInfo.login;
    console.log(this.IsRegisteredUser);
    this.isTorchSupported = this.webRtcService.isTorchSupported;
  }

  //Filter Change Function
  async onChange($event) {
    this.classVariable = "video-item " + $event.target.value;
    switch (this.classVariable) {
      case "none":
        this.filterFunc = "none";
        break;
      case "blur":
        this.filterFunc = "blur(4px)";
        break;
      case "brightness":
        this.filterFunc = "brightness(0.30)";
        break;
      case "contrast":
        this.filterFunc = "contrast(180%)";
        break;
      case "grayscale":
        this.filterFunc = "grayscale(100%)";
        break;
      case "huerotate":
        this.filterFunc = "hue-rotate(180deg)";
        break;
      case "invert":
        this.filterFunc = "invert(100%)";
        break;
      case "opacity":
        this.filterFunc = "opacity(50%)";
        break;
      case "saturate":
        this.filterFunc = "saturate(7)";
        break;
      case "sepia":
        this.filterFunc = "sepia(100%)";
        break;
      case "shadow":
        this.filterFunc = "drop-shadow(8px 8px 10px green)";
        break;


    }    console.log($event.target.value);
  }

  async captureImage() {
    await this.uploadImage(await this.photoService.captureImage(this.filterFunc));
  }

  async uploadImage(base64: string) {

    let ref = this;
    this.presentAlertConfirm(async () => {
      let userInfo = await ref.authService.GetUserInfoLocal();

      if (userInfo.login) {
        await ref.modalService.Present(FolderListPage, async () => {

          let selectedFolders = ref.modalService.outputSharedProp as Folder[];
          if (selectedFolders.length > 0) {
            await ref.photoService.SaveBaseImage(selectedFolders, "", base64);
            ref.toastService.presentToast("uploaded", 500)
          } else {
            ref.toastService.presentToast("upload canceled", 500)
          }

        });
      } else {

        await ref.photoService.SaveBaseImage(userInfo.Name, userInfo.SharedFolderId, base64);
        ref.toastService.presentToast("uploaded", 500)
        ref.photoService.loadImageByUniqueSharedId(userInfo.SharedFolderId);
      }
    })

  }
  async switchCamera() {

    if (this.camView === CameraView.Back)
      this.camView = CameraView.Front;
    else
      this.camView = CameraView.Back;

    this.webRtcService.switchCameraFacingMode(this.camView)
  }

  async switchLight() {
    this.isLightOn = !this.isLightOn;
    this.webRtcService.switchTouchLight(this.isLightOn)
  }

  async presentJoinNearbyPrompt() {
    this.navController.navigateForward('main/join-nearby');
  }

  async presentAddFolderPrompt() {
    this.navController.navigateForward('main/create-folder');
  }
  async GoToMyFolders() {
    this.navController.navigateForward('/main/folder');

  }
  async GoToGallery() {
    this.navController.navigateForward('/main/tab2');

  }
  async GoToMapView() {
    this.navController.navigateForward('/map-view');

  }
  async Logout() {
    await Storage.clear();
    this.folderService.Folders = null
    this.navController.navigateForward('/map-view');
  }

  async presentAlertConfirm(onOk) {
    const alert = await this.alertController.create({
      header: 'Save Image?',
      message: 'Image can\t be recovered once cancelled',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: onOk
        }
      ]
    });

    await alert.present();
  }


  async loadFile(event) {
    await this.uploadImage(await this.photoService.convertBlobToBase64(event.target.files[0]))
  }


}
