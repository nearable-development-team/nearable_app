import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { NearbyIconListPage } from './nearby-icon-list.page';

describe('FolderListPage', () => {
  let component: NearbyIconListPage;
  let fixture: ComponentFixture<NearbyIconListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NearbyIconListPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NearbyIconListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
