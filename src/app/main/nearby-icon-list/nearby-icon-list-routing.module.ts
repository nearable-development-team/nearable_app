import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NearbyIconListPage } from './nearby-icon-list.page';

const routes: Routes = [
  {
    path: '',
    component: NearbyIconListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NearbyIconListPageRoutingModule { }
