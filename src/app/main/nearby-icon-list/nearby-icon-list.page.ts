import { Component, OnInit } from '@angular/core';
import { FolderService } from 'src/app/services/folder/folder.service';
import { ModalService } from 'src/app/services/modal/modal.service';
import "hammerjs"
import { GeoLocationService } from 'src/app/services/geo-location/GeoLocation.service';
@Component({
  selector: 'app-nearby-icon-list',
  templateUrl: './nearby-icon-list.page.html',
  styleUrls: ['./nearby-icon-list.page.scss'],
})
export class NearbyIconListPage implements OnInit {
  public IconfolderNames: string[] = [];
  public Icons: string[] = [];
  public SelectedFolder: string;
  public SelectedIcon: string;
  private IconsData: any;
  public isFolderView: boolean = true;
  constructor(
    public folderService: FolderService,
    public modalService: ModalService,
    private geoLocationService: GeoLocationService,
  ) {
  }

  async ngOnInit() {
    this.IconsData = await this.geoLocationService.getMapMarkerIcons();
    this.IconfolderNames = this.IconsData.FolderNames;
    console.log(this.IconsData);
  }

  async ionViewWillEnter() {
  }

  async GetIconList(folderName: string) {
    this.isFolderView = false;
    this.SelectedFolder = folderName;
    this.Icons = this.IconsData.Icons[folderName];
  }

  async GoToFolderView() {
    this.isFolderView = true;
    this.SelectedIcon = undefined;
  }
  async SelectIcon(iconName: string) {
    this.SelectedIcon = iconName;
  }
  async SelectIconAndClose() {
    this.modalService.outputSharedProp = this.SelectedFolder + "/" + this.SelectedIcon;
    await this.modalService.Dismiss();
  }
  async Cancel() {
    this.modalService.outputSharedProp = "";
    await this.modalService.Dismiss();
  }
}
