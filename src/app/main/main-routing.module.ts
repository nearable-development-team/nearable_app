import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPage } from './main.page';

const routes: Routes = [
  {
    path: '',
    component: MainPage,
    children: [
      {
        path: '',
        redirectTo: 'camera-screen-new',
        pathMatch: 'full'
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () => import('./tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'folder',
        children: [
          {
            path: '',
            loadChildren: () => import('./folder/folder.module').then(m => m.FolderPageModule)
          }
        ]
      },
      {
        path: 'create-folder',
        children: [
          {
            path: '',
            loadChildren: () => import('./create-folder/create-folder.module').then(m => m.CreateFolderPageModule)
          }
        ]

      },
      {
        path: 'create-folder-success',
        children: [
          {
            path: '',
            loadChildren: () => import('./create-folder-success/create-folder-success.module').then(m => m.CreateFolderSuccessPageModule)
          }
        ]

      },
      {
        path: 'camera-screen',
        children: [
          {
            path: '',
            loadChildren: () => import('./camera-screen/camera-screen.module').then(m => m.CameraScreenPageModule)
          }
        ]

      },
      {
        path: 'camera-screen-new',
        children: [
          {
            path: '',
            loadChildren: () => import('./camera-screen-new/camera-screen-new.module').then(m => m.CameraScreenNewPageModule)
          }
        ]

      },
      {
        path: 'join-nearby',
        children: [
          {
            path: '',
            loadChildren: () => import('./join-nearby/join-nearby.module').then(m => m.JoinNearbyPageModule)
          }
        ]

      },

    ]
  },
  {
    path: 'folder-list',
    loadChildren: () => import('./folder-list/folder-list.module').then(m => m.FolderListPageModule)
  },  {
    path: 'merchant',
    loadChildren: () => import('./merchant/merchant.module').then( m => m.MerchantPageModule)
  },
  {
    path: 'thanks',
    loadChildren: () => import('./thanks/thanks.module').then( m => m.ThanksPageModule)
  },
  {
    path: 'checkoutpage',
    loadChildren: () => import('./checkoutpage/checkoutpage.module').then( m => m.CheckoutpagePageModule)
  },
  {
    path: 'passwordscreen',
    loadChildren: () => import('./passwordscreen/passwordscreen.module').then( m => m.PasswordscreenPageModule)
  },
  {
    path: 'emailscreen',
    loadChildren: () => import('./emailscreen/emailscreen.module').then( m => m.EmailscreenPageModule)
  },
  {
    path: 'businessscreen',
    loadChildren: () => import('./businessscreen/businessscreen.module').then( m => m.BusinessscreenPageModule)
  },
  {
    path: 'ar',
    loadChildren: () => import('./ar/ar.module').then( m => m.ARPageModule)
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainPageRoutingModule { }
