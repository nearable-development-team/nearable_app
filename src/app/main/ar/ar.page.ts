import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-ar',
  templateUrl: './ar.page.html',
  styleUrls: ['./ar.page.scss'],
})
export class ARPage implements OnInit {
  public IframeUrl: string;
  public pUrl: any;
  constructor(public sanitizer: DomSanitizer) { }

  async ngOnInit() {

    this.IframeUrl = "/assets/ar/6.html";
    await this.urlchanged(this.IframeUrl);
  }
  async urlchanged(url) {
    console.log(url);
    this.pUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    console.log(this.pUrl);
  }
}
