import { Component, OnInit } from '@angular/core';
import { FolderService } from 'src/app/services/folder/folder.service';
import { Folder } from 'src/app/interface/interface';
import { ModalService } from 'src/app/services/modal/modal.service';
import "hammerjs"
import { ToastService } from 'src/app/services/toast/toast.service';
@Component({
  selector: 'app-folder-list',
  templateUrl: './folder-list.page.html',
  styleUrls: ['./folder-list.page.scss'],
})
export class FolderListPage implements OnInit {
  public folders: Folder[]
  public AllCheckbox: boolean;
  constructor(
    public folderService: FolderService,
    public modalService: ModalService,
    private toastService: ToastService,
  ) {
  }

  ngOnInit() {

  }

  async ionViewWillEnter() {
    this.AllCheckbox = false;
    this.folders = JSON.parse(JSON.stringify(this.folderService.Folders));
  }

  async longPress(folder: Folder) {
    folder.isSelected = !folder.isSelected;
    console.log(folder)
  }
  async SelectFolder() {
    let selectedFolders = this.folders.filter((x) => { return x.isSelected });
    if (selectedFolders.length > 0) {
      this.modalService.outputSharedProp = selectedFolders;
      await this.modalService.Dismiss();
    } else {
      this.toastService.presentToast("No folder selected!!", 1000)
    }
  }
  getSelectFolderCount() {
    return (this.folders || []).filter((x) => { return x.isSelected }).length;
  }
  async CancelSave() {
    this.modalService.outputSharedProp = [];
    await this.modalService.Dismiss();
  }
  isAnyFolderSelected() {
    return this.getSelectFolderCount() > 0
  }
  SelectAllFolder() {
    for (const folder of this.folders) {
      folder.isSelected = this.AllCheckbox;
    }
  }
}
