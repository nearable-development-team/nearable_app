import { Component, OnInit } from '@angular/core';
import { FolderService } from '../../services/folder/folder.service';
import { Folder } from '../../interface/interface';
import { NavController, AlertController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast/toast.service';
import { WebRtcService } from 'src/app/services/WebRTC/webrtc.service';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { ShareService } from 'src/app/services/shareService/ShareService.service';


@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public SearchKeyword = "";
  public isShared = false;
  public isArchived = false;
  public folders: Folder[]
  public IsWebRTCSupported: boolean;
  constructor(
    public folderService: FolderService,
    private toastService: ToastService,
    private navController: NavController,
    private webRtcService: WebRtcService,
    private alertController: AlertController,
    private photoService: PhotoService,
    private shareService: ShareService,
  ) {
  }


  async ionViewWillEnter() {
    let ref = this;
    ref.webRtcService.init('folder-video');
    this.IsWebRTCSupported = this.webRtcService.isWebRTCSupported;
    ref.folderService.GetAllFolder(true)
  }

  async ngOnInit() {

  }
  public async ViewGallery(folder: Folder) {
    this.navController.navigateForward('main/tab2', { queryParams: { folder: JSON.stringify(folder) } });
  }
  public ShowOptions(folder: Folder) {
    this.shareService.Share(folder.SharedFolderId);
  }



  public folderFilter(tabFilter: string) {
    if (tabFilter == 'archived') {
      this.isArchived = true;
      this.isShared = false;
    }
    else if (tabFilter == "sharedOnly") {
      this.isArchived = false;
      this.isShared = true;
    } else {
      this.isShared = this.isArchived = false;
    }
    console.log(tabFilter);
  }
  public async markArchived(folder: Folder) {
    let ref = this;
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure to archive it?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: () => {
            ref.folderService.ArchiveFolder(folder).then(() => {
              this.toastService.presentToast('Archived.');
            })
          }
        }
      ]
    });

    await alert.present();
  }

  async loadFile(event, folder) {
    let ref = this;
    let base64 = await this.photoService.convertBlobToBase64(event.target.files[0])
    let selectedFolders = [folder];
    if (selectedFolders.length > 0) {
      await ref.photoService.SaveBaseImage(selectedFolders, "", base64);
      ref.toastService.presentToast("uploaded", 500)
    } else {
      ref.toastService.presentToast("upload canceled", 500)
    }

  }
}
