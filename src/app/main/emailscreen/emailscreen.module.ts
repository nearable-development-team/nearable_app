import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmailscreenPageRoutingModule } from './emailscreen-routing.module';

import { EmailscreenPage } from './emailscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmailscreenPageRoutingModule
  ],
  declarations: [EmailscreenPage]
})
export class EmailscreenPageModule {}
