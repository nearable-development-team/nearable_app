import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal/modal.service';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-emailscreen',
  templateUrl: './emailscreen.page.html',
  styleUrls: ['./emailscreen.page.scss'],
})
export class EmailscreenPage implements OnInit {

  constructor(private modalService: ModalService,
    private toastService: ToastService) { }

  ngOnInit() {
  }
  async emailSend() {
    this.modalService.Dismiss();
    this.toastService.presentToast("Email Sent !!")
  }

}
