import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmailscreenPage } from './emailscreen.page';

describe('EmailscreenPage', () => {
  let component: EmailscreenPage;
  let fixture: ComponentFixture<EmailscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailscreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmailscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
