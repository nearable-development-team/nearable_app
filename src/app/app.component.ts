import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SwUpdate } from '@angular/service-worker';
import { ToastService } from './services/toast/toast.service';
declare var Stripe;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private swUpdate: SwUpdate,
    private toastService: ToastService,
  ) {
    this.swUpdate.available.subscribe(event => {
      console.log('current version is', event.current);
      console.log('available version is', event.available);
      this.activateUpdate()
    });
    this.swUpdate.activated.subscribe(event => {
      console.log('old versionq was', event.previous);
      console.log('new version is', event.current);
    });

    this.initializeApp();

  }

  initializeApp() {
    var ref = this;
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.geoLocationService.WatchPosition((f) => {
      //   console.log(f);
      // }, (f) => {
      //   console.log(f);
      // })
      Stripe.setPublishableKey("pk_test_51H4a7DLguoKk1fRsWvK87CyVG7Aosp4jLznIaL3ZYp9flZCXzRfoPEZssL8wNaHnyDPumfsB1aADyIDEU4hIfGFV00nbWFyYpp");
      setInterval(() => {
        ref.swUpdate.checkForUpdate();
      }, 10000);

    });
  }

  checkForUpdate() {
    console.log('[App] checkForUpdate started')
    this.swUpdate.checkForUpdate()
      .then(() => {
        console.log('[App] checkForUpdate completed')
      })
      .catch(err => {
        console.error(err);
      })
  }

  activateUpdate() {
    console.log('[App] activateUpdate started')
    this.swUpdate.activateUpdate()
      .then(() => {
        console.log('[App] activateUpdate completed')
        this.toastService.presentToast(`new version found!! Hit Refresh`, 10000);
      })
      .catch(err => {
        console.error(err);
      })
  }


}
