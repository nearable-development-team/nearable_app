export interface DeviceInfo {
    memUsed?: number;
    diskFree?: number;
    diskTotal?: number;
    model?: string;
    osVersion?: string;
    appVersion?: string;
    appBuild?: string;
    platform?: string;
    manufacturer?: string;
    uuid?: string;
    batteryLevel?: number;
    isCharging?: boolean;
    isVirtual?: boolean;
}

export interface Users {
    id?: number;
    name?: string;
    email?: string;
    photo?: string;
    login_type?: boolean; // 0: google, 1: apple
    status?: boolean; // 1: active, 0: block
    created_at?: Date;
}

export interface Host {
    id?: number;
    user_id?: number;
    event_name?: string;
    share_code?: string;
    group?: string;
    locked?: boolean;
    created_at?: Date;
    status?: boolean;
}

export interface Favourites {
    id?: number;
    user_id?: number;
    folder_url?: string;
    created_at?: Date;
}

export interface Sessions {
    id?: number;
    user_id?: number;
    email?: string;
    password?: string;
    token?: string;
    created_at?: Date;
}

export interface History {
    id?: number;
    user_id?: number;
    login_at?: Date;
}



export interface Card {
    username?: string;
    data?: Date;
    name?: string;
}
/////////////////////////////////////
export interface Folder {
    Id?: number;
    Name: string;
    SharedFolderId: string;
    AuthorName: string;
    TotalFiles?: number;
    ModifiedDate: string;
    AuthorId: string;
    isSelected?: boolean;
    UserFolderId: number;
    IsArchived: string;
    Longitude: number;
    Latitude: number;
    FolderIcon: string;
    IsPrivateFence: string;
}


export interface Response {
    Status: number;
    Result: any
}

export interface UserDetail {
    Id?: string,
    Email: string,
    Name: string
    Image: string
}

export interface FileUploadResponse {
    Name: string,
    Path: string
}

export interface FileUploadRequest {
    Name: string,
    Base64: string,
    UserId?: string,
    SharedFolderId?: string,
    FolderName: any,
    UploadedByUserId?: string
}

export interface GetFilesRequest {
    UserId: string,
    FolderName: string,
    FolderId: number
}

export interface Photo {
    Name?: string;
    FilePath?: string;
    UserName?: string;
}

export interface GetFilesResponse {
    Images: Photo[],
    BasePath: string,
}
export interface GetFoldersResponse {
    Folders: Folder[],
    BasePath: string,
}
export interface UniqueFolderRequest {
    SharedFolderId: string,
}
export interface AddSharedFolderRequest {
    SharedFolderId: string,
    UserId: number,
}

export interface UserLocation {
    accuracy: number,
    Latitude: any,
    Longitude: any
}
export interface MarkerInfo {
    Latitude: any,
    Longitude: any,
    Title: string,
    Label: string,
    Icon: string,
    AdditionalInfo: any
}

export interface CreateCardRequest {
    CardHolderEmail: string,
    Token: string,
    CardHolderName: string,
    Amount: number,
    Password: string,
    PersonalMessage: string,
    RecLastName: string,
    RecAddress: string,
    RecCountry: string,
    RecCity: string,
    RecState: string,
    RecPostalCode: string,
    PlaceId: string,
    PlaceName: string,
    UserId: string,
    Longitude: string,
    Latitude: string

}