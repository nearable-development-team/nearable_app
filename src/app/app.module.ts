import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FolderListPageModule } from './main/folder-list/folder-list.module';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { IonicGestureConfig } from "./gestures/ionic-gesture-config";
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { SharedComponentModule } from './component/shared-component.module';
import { NearbyIconListPageModule } from './main/nearby-icon-list/nearby-icon-list.module';
import { CheckoutpagePageModule } from './main/checkoutpage/checkoutpage.module';
import { BusinessscreenPageModule } from './main/businessscreen/businessscreen.module';
import { PasswordscreenPageModule } from './main/passwordscreen/passwordscreen.module';
import { EmailscreenPageModule } from './main/emailscreen/emailscreen.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    FolderListPageModule,
    NearbyIconListPageModule,
    SharedComponentModule,
    CheckoutpagePageModule,
    BusinessscreenPageModule,
    PasswordscreenPageModule,
    CheckoutpagePageModule,
    EmailscreenPageModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    NgxIonicImageViewerModule],
  providers: [
    StatusBar,
    GooglePlus,
    SplashScreen,
    { provide: IonicRouteStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
