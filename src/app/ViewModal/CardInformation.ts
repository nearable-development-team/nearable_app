export class CheckoutPageViewModal {
    /**
     *
     */
    constructor() {
        this.ServiceCharge = 5;
    }
    CardNumber: string;
    CCV: string;
    ExpiryYear: string;
    ExpiryMonth: string;
    Token: string;
    Amount: number;
    ServiceCharge: number;
    CardHolderEmail: string;
    CardHolderName: string;
    PersonalMessage: string;
    RecFirstName: string;
    RecAddress: string;
    RecLastName: string;
    RecCountry: string;
    RecCity: string;
    RecState: string;
    RecPostalCode: string;

}