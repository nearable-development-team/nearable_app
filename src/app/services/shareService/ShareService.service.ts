import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { ActionSheetController } from '@ionic/angular';
import { ToastService } from '../toast/toast.service';
const { Clipboard } = Plugins;
const navigator = window.navigator as any;
@Injectable({
  providedIn: 'root'
})
export class ShareService {
  constructor(
    private toastService: ToastService,
    private actionSheetController: ActionSheetController) { }

  /**
   * Share
   */
  public async Share(code: string) {
    try {
      await navigator.share({
        title: "Share Code with friend ",
        text: code
      })

    } catch (err) {

      console.log(`Couldn't share ${err}`);
      this.presentActionSheet(code);
    }
  }

  private async presentActionSheet(code: string) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [{
        text: 'Copy Id: ' + code,
        icon: 'copy',
        role: 'copy',
        handler: () => {
          Clipboard.write({
            string: code
          });

          this.toastService.presentToast('Copied.');
        }
      }]
    });
    await actionSheet.present();
  }
}
