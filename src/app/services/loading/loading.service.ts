import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loader=null;
  constructor(
    private loadingCtrl: LoadingController
  ) { }

public  async presentLoading(msg:string="") {
    this.loader = await this.loadingCtrl.create({
      message: msg,
    });
    this.loader.present();
  }
 public async dismiss() {
    if(this.loader!=null){
      this.loader.dismiss();
      this.loader=null
    }
  }
}
