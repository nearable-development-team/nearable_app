import { Injectable } from '@angular/core';
import { Folder, UniqueFolderRequest, GetFoldersResponse, AddSharedFolderRequest, Response, UserLocation } from './../../interface/interface';
import { HttpService } from '../http/Http.service';
import { Plugins } from '@capacitor/core';
import { AuthService } from '../auth/auth.service';
const { Storage } = Plugins;

@Injectable({
    providedIn: 'root'
})
export class FolderService {
    public Folders: Folder[] = null;
    public BasePath: string = "";
    private userInfo;
    constructor(private httpService: HttpService,
        public authService: AuthService,
    ) {
        this.GetAllFolder();
    }

    public async AddFolder(folderName: string, longitude: any, latitude: any,
        fenceAreaInFeet: number, folderIcon: string, isPrivateFence: boolean): Promise<Folder> {
        var folder = {
            FolderName: folderName,
            UserId: this.userInfo.Id,
            Longitude: longitude,
            Latitude: latitude,
            FenceAreaInFeet: fenceAreaInFeet,
            FolderIcon: folderIcon,
            IsPrivateFence: isPrivateFence
        };
        return await this.httpService.Post("addfolderbyUser", folder) as Folder;
    }
    public async AddSharedFolder(sharedId: string) {

        var addSharedFolderRequest: AddSharedFolderRequest = {
            SharedFolderId: sharedId,
            UserId: this.userInfo.Id
        };
        await this.httpService.Post("JoinByUnquieFolderId", addSharedFolderRequest);
    }
    public async IsSharedFolderIdExists(uniqueFolderId: string): Promise<Folder> {
        return await this.httpService.Post<UniqueFolderRequest, Folder>("searchsharedfolderid", { SharedFolderId: uniqueFolderId }) as Folder;
    }

    public async GetAllFolder(forceRefresh: boolean = false) {
        this.userInfo = await this.authService.GetUserInfoLocal();
        if (forceRefresh || this.Folders == null) {
            let response = await this.httpService.Post<any, GetFoldersResponse>("getfoldersbyuser", { UserId: this.userInfo.Id });

            this.Folders = response.Folders;
            this.BasePath = response.BasePath;
            return this.Folders;

        } else {
            return this.Folders
        }
    }

    /**
     * GetFoldersByLocation
     */
    public async GetFoldersByLocation(userLocation: UserLocation) {
        return await (await this.httpService.Post<UserLocation, GetFoldersResponse>("getfolderbylocation", userLocation)).Folders;

    }
    /**
     * SearchFolder
     */
    public SearchFolder(keyword: string, isArchive: boolean, IsShared: boolean) {
        keyword = keyword || "";
        let ref = this;
        if (this.Folders) {
            return this.Folders.filter(item => {
                return item.Name.toLowerCase().indexOf(keyword.toLowerCase()) > -1
                    && (!isArchive || ref.isFolderAchived(item))
                    && (!IsShared || !ref.IsCurrentUserAuthor(item));
            });
        }

    }
    /**
     * IsCurrentUserAuthor
     */
    public IsCurrentUserAuthor(folder: Folder): boolean {
        return this.userInfo.Id == folder.AuthorId;
    }
    public GetTotalImages(folder: Folder) {
        if (folder.TotalFiles && folder.TotalFiles > 0) {
            if (folder.TotalFiles > 99) {
                return "99+"
            }
        } else {
            return ""
        }
        return folder.TotalFiles;
    }

    public async ArchiveFolder(folder: Folder): Promise<Response> {
        folder.IsArchived = "1";
        return await this.httpService.Post("ArchiveFolder", { Id: folder.UserFolderId }) as Response;
    }
    public isFolderAchived(folder: Folder): boolean {
        return folder.IsArchived == "1";
    }
}
