import { Injectable } from '@angular/core';
import { HttpService } from '../http/Http.service';
import { Response, CreateCardRequest } from 'src/app/interface/interface';

@Injectable({
  providedIn: 'root'
})
export class StripeService {
  constructor(
    private httpService: HttpService
  ) { }

  public async UploadCardDetails(cardRequest: CreateCardRequest) {
    return await this.httpService.Post("ChargeAndCreateVirtualCard", cardRequest) as Response;
  }
}