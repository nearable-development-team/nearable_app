import { Injectable } from '@angular/core';
import * as DetectRTC from 'detectrtc';
import { CameraView } from '../../interface/enums';

declare global {
    interface Window { stream: any; }
}

@Injectable({
    providedIn: 'root'
})
export class WebRtcService {
    public isWebRTCSupported: boolean = false;
    public isTorchSupported: boolean = false;

    private video: any;
    private isTorchOn: boolean = true;
    private currentFacingMode: CameraView = CameraView.Back;
    private element: string = "";
    private vedioTrack: any;
    constructor() {
        console.log("service loded")
        return
        let ref = this;
        DetectRTC.load(function () {
            ref.isWebRTCSupported = DetectRTC.isWebRTCSupported;

            console.log(
                'RTC Debug info: ' +
                '\n OS:                   ' +
                DetectRTC.osName +
                ' ' +
                DetectRTC.osVersion +
                '\n browser:              ' +
                DetectRTC.browser.fullVersion +
                ' ' +
                DetectRTC.browser.name +
                '\n is Mobile Device:     ' +
                DetectRTC.isMobileDevice +
                '\n has webcam:           ' +
                DetectRTC.hasWebcam +
                '\n has permission:       ' +
                DetectRTC.isWebsiteHasWebcamPermission +
                '\n getUserMedia Support: ' +
                DetectRTC.isGetUserMediaSupported +
                '\n isWebRTC Supported:   ' +
                DetectRTC.isWebRTCSupported +
                '\n WebAudio Supported:   ' +
                DetectRTC.isAudioContextSupported +
                '\n is Mobile Device:     ' +
                DetectRTC.isMobileDevice,
            );
        });
    }

    public init(ele: string) {
        if (!this.isWebRTCSupported)
            return;
        this.element = ele;
        this._init();
    }
    public getCameraView(): CameraView {
        if (!this.isWebRTCSupported)
            return;
        return this.currentFacingMode;
    }
    private stopExitingStream() {
        if (!this.isWebRTCSupported)
            return;
        if (window.stream) {
            window.stream.getTracks().forEach(function (track) {
                track.stop();
            });
        }
    }
    private _init() {
        if (!this.isWebRTCSupported)
            return;
        let ref = this;
        this.video = document.getElementById(this.element);
        // stop any active streams in the window
        this.stopExitingStream()

        var size = 1280;

        var constraints = {
            audio: false,
            video: {
                width: { ideal: size },
                height: { ideal: size },
                facingMode: this.currentFacingMode,
            },
        };

        navigator.mediaDevices
            .getUserMedia(constraints)
            .then(function (stream) {
                window.stream = stream; // make stream available to browser console
                ref.video.srcObject = stream;
                ref.vedioTrack = window.stream.getVideoTracks()[0];
                ref.video.addEventListener('loadedmetadata', (e) => {
                    ref.isTorchSupported = ref.vedioTrack.getCapabilities().torch
                });


                const settings = ref.vedioTrack.getSettings();
                let str = JSON.stringify(settings, null, 4);
                console.log('settings ' + str);

                //return navigator.mediaDevices.enumerateDevices();
            })
            .catch(
                function (error) {
                    console.log(error);

                    if (error === 'PermissionDeniedError') {
                        alert('Permission denied. Please refresh and give permission.');
                    }
                });
    }

    public switchCameraFacingMode(camView: CameraView) {
        if (!this.isWebRTCSupported)
            return;
        this.currentFacingMode = camView;
        this._init();
    }
    public switchTouchLight(switchOn: boolean) {
        if (!this.isWebRTCSupported)
            return;
        this.isTorchOn = switchOn;
        if (switchOn == true) {
            this.applyTorchConstraints()
        } else {
            this._init();
        }

    }
    private applyTorchConstraints() {
        if (!this.isWebRTCSupported)
            return;
        this.vedioTrack.applyConstraints({
            advanced: [{ torch: this.isTorchOn }]
        });
    }

    public async getPhoto(filterClassName: string = ""): Promise<any> {
        if (!this.isWebRTCSupported)
            return;
        return new Promise((resolve) => {
            let canvas = document.createElement("canvas");
            canvas.width = this.video.videoWidth;
            canvas.height = this.video.videoHeight;

            let context = canvas.getContext('2d');
            context.filter = filterClassName;
            context.drawImage(this.video, 0, 0, canvas.width, canvas.height);
            canvas.toBlob(function (blob) {
                resolve(blob);

            }, 'image/png');
        })
    }

}
