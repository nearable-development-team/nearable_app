import { TestBed } from '@angular/core/testing';

import { WebRtcService } from './webrtc.service';

describe('FolderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebRtcService = TestBed.get(WebRtcService);
    expect(service).toBeTruthy();
  });
});
