import { Injectable } from '@angular/core';
import { UserLocation } from 'src/app/interface/interface';
import { HttpService } from '../http/Http.service';

@Injectable({
    providedIn: 'root'
})
export class GeoLocationService {

    constructor(private httpService: HttpService) { }

    public GetLocation(): Promise<UserLocation> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition((loc: any) => {
                resolve({ accuracy: loc.coords.accuracy, Latitude: loc.coords.latitude, Longitude: loc.coords.longitude })
            }, (ar) => {
                console.log(ar);
                reject(null)
            });
        });
    }

    public WatchPosition(sCb, eCb) {
        navigator.geolocation.watchPosition(sCb || (() => { }), eCb || (() => { }))
    }

    public async getMapMarkerIcons(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            var folderIconList = await this.httpService.Get<any>("getmapmarkericons");
            var iconFoldernames = Object.keys(folderIconList);
            resolve({ FolderNames: iconFoldernames, Icons: folderIconList })
        });

    }
}
