import { Injectable } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { Plugins, CameraResultType, Capacitor, FilesystemDirectory, CameraPhoto, CameraSource } from '@capacitor/core';
import { Photo, FileUploadResponse, FileUploadRequest, UserDetail, GetFilesRequest, GetFilesResponse, UniqueFolderRequest, Folder } from './../../interface/interface';
import { HttpService } from '../http/Http.service';
import { WebRtcService } from '../WebRTC/webrtc.service';
import { AuthService } from '../auth/auth.service';

const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
    providedIn: 'root'
})
export class PhotoService {
    private platform: Platform;
    public currentFolder: Folder;
    constructor(
        platform: Platform,
        public navController: NavController,
        private httpService: HttpService,
        private webRtcService: WebRtcService,
        public authService: AuthService
    ) {
        this.platform = platform;

    }

    private async readAsBase64(cameraPhoto: CameraPhoto) {
        if (this.platform.is('hybrid')) {
            const file = await Filesystem.readFile({
                path: cameraPhoto.path
            });
            return file.data;
        }
        else {
            const response = await fetch(cameraPhoto.webPath);
            return await response.blob();
        }
    }

    convertBlobToBase64 = (blob: Blob): Promise<string> => new Promise((resolve, reject) => {
        const reader = new FileReader;
        reader.onerror = reject;
        reader.onload = () => {
            resolve(reader.result as string);
        };
        reader.readAsDataURL(blob);
    });

    public async captureImage(filterClassName: string = ""): Promise<string> {

        return new Promise(async (resolve, reject) => {
            if (this.platform.is('hybrid')) console.log("running in hybrid...");
            resolve(await this.convertBlobToBase64(await this.webRtcService.getPhoto(filterClassName) as Blob));
        });
    }

    public async SaveBaseImage(currentFolderName: any, sharedFolderId: string,
        base64: string, authorId: string = ""): Promise<FileUploadResponse> {
        return new Promise(async (resolve, reject) => {
            const fileName = new Date().getTime() + '.png';
            var userInfo = await this.authService.GetUserInfoLocal();
            let fileUploadRequest: FileUploadRequest = {
                Name: fileName,
                Base64: base64,
                FolderName: currentFolderName
            };
            let requestType = ''
            if (!sharedFolderId) {
                fileUploadRequest.UserId = authorId;
                requestType = "uploadbase64fileTomultifolder";
            } else {
                fileUploadRequest.SharedFolderId = sharedFolderId;
                requestType = 'uploadbase64filebysharedid';
            }

            if (userInfo.login) {
                fileUploadRequest.UploadedByUserId = userInfo.Id;
            } else {
                fileUploadRequest.UploadedByUserId = "0";
            }

            let response = await this.httpService
                .Post<FileUploadRequest, FileUploadResponse>(requestType, fileUploadRequest);
            resolve(response as FileUploadResponse);
        });
    }

    public async loadImageFromServer(FolderName: string, folderId: number, authorId: string)
        : Promise<GetFilesResponse> {
        return new Promise(async (resolve, reject) => {
            let data = await this.httpService
                .Post<GetFilesRequest, GetFilesResponse>("getimages",
                    { UserId: authorId, FolderName: FolderName, FolderId: folderId })
            resolve(data as GetFilesResponse)
        });

    }

    public async loadImageByUniqueSharedId(FolderSharedId: string): Promise<GetFilesResponse> {
        return new Promise(async (resolve, reject) => {
            let data = await this.httpService.Post<UniqueFolderRequest, GetFilesResponse>("getimagebyuniquefolderid", { SharedFolderId: FolderSharedId })
            resolve(data as GetFilesResponse)
        });

    }




}
