import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public outputSharedProp: any;
  private modal: HTMLIonModalElement
  constructor(
    private modalCtrl: ModalController
  ) { }

  async Present(Component: any, cb: any, props: any = null) {
    this.modal = await this.modalCtrl.create({
      component: Component,
      backdropDismiss: false,
      componentProps: props
    });
    this.modal.onDidDismiss().then((d: any) => cb(this.outputSharedProp));
    return await this.modal.present();
  }

  async Dismiss() {
    this.modal.dismiss({
      'dismissed': true
    });
  }
}
