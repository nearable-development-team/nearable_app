import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseApp } from '@angular/fire';

@Injectable({
    providedIn: 'root'
})
export class FirebaseDbService {

    constructor(
        private firestore: AngularFirestore,
        private firebase: FirebaseApp,
    ) { }

    public async Add(Name: string, record) {
        return await this.firestore.collection(Name).add(record);
    }

    public async GetAll(Name: string) {
        //return this.firestore.collection(Name).snapshotChanges();

        var data = [];
        await this.firebase.firestore().collection(Name).get()
        .then(async function(querySnapshot) {
            querySnapshot.forEach(await function(doc) {
                data.push({i: doc.id, d: doc.data()});
            });
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
        console.log(data);
        return data;
    }

    public async Update(Name: string, recordID, record) {
        this.firestore.doc(Name + '/' + recordID).update(record);
    }

    public async deleteUser(Name: string, record_id) {
        this.firestore.doc(Name + '/' + record_id).delete();
    }

}
