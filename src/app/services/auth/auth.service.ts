import { Injectable } from '@angular/core';
import { HttpService } from '../http/Http.service';
import { UserDetail } from 'src/app/interface/interface';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: any = null;

  constructor(private httpService: HttpService) {

  }

  async SaveUser(user: UserDetail): Promise<UserDetail> {
    var saveUserResponse = await this.httpService.Post("adduser", user);
    console.log(saveUserResponse);
    this.user = saveUserResponse;
    return this.user;
  }
  async SaveUserLocalState(user: any) {
    await Storage.set({
      key: 'userInfo',
      value: JSON.stringify(user)
    });
  }



  async GetUserInfoLocal() {

    try {
      var str = await Storage.get({
        key: 'userInfo'
      });
      if (str.value) {
        return JSON.parse(str.value);
      }
    }
    catch (e) {
    }

    return { login: false };
  }

}
