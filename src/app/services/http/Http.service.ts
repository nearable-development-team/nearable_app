import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public _baseUrl: string = "https://nearable.app/ServerApi/api.php?type=";//"http://localhost/nearable/nearable_server/api.php?type=";//

  constructor() {


  }
  /**
   * Get
   */
  public async Post<I, O>(relativeUrl: string, payload: I): Promise<O> {

    return new Promise((resolve, reject) => {
      console.log(relativeUrl, payload);
      axios.post(this._baseUrl + relativeUrl, payload, {

        // `onUploadProgress` allows handling of progress events for uploads
        onUploadProgress: function (progressEvent) {
          // Do whatever you want with the native progress event
          console.log(progressEvent)
        },
        // `onDownloadProgress` allows handling of progress events for downloads
        onDownloadProgress: function (progressEvent) {
          // Do whatever you want with the native progress event
          console.log(progressEvent)
        }
      })
        .then(res => {
          console.log(res.data.Result);
          if (res.data.Status === 1) {
            resolve(res.data.Result as O)
          } else {
            reject({ ErrorMessage: res.data.Message })
          }

        })
        .catch(error => {
          reject({ ErrorMessage: error })
          console.log(error);
        })
    })

  }

  public async Get<O>(relativeUrl: string): Promise<O> {
    return new Promise((resolve, reject) => {
      console.log(relativeUrl);
      axios.get(this._baseUrl + relativeUrl, {})
        .then(res => {
          resolve(res.data.Result as O)
        })
        .catch(error => {
          reject({ ErrorMessage: error })
        })

    })
  }
}
