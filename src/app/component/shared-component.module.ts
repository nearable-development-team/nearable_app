import { NgModule } from '@angular/core';
import { GoogleMapComponent } from './google-map/google-map.component';


@NgModule({
    imports: [],
    declarations: [GoogleMapComponent],
    exports: [GoogleMapComponent]
})
export class SharedComponentModule { }
