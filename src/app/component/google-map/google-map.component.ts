import { Component, OnInit, Input, Renderer2, ElementRef, Inject, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Network, Geolocation } from '@capacitor/core';
import { GeoLocationService } from 'src/app/services/geo-location/GeoLocation.service';
import { UserLocation, MarkerInfo } from 'src/app/interface/interface';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
})
export class GoogleMapComponent implements OnInit {

  @Output()
  onClickViewFolder: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  onClickCreateGioGift: EventEmitter<any> = new EventEmitter<any>();

  private apiKey: string = "AIzaSyDT01oe_DTxTZ5Ts8ALfkeTCAYuz-S9lJ8";
  private mapCircle: any;
  private map: any;
  private infowindow: any;
  private markers: any = {};
  static mapsLoaded: boolean = false;
  static currentLocation: UserLocation = null;
  private networkHandler = null;
  constructor(private renderer: Renderer2, private element: ElementRef, @Inject(DOCUMENT) private _document, private geoLocationService: GeoLocationService, private loadingService: LoadingService) {
    console.log(this.element);
  }

  ngOnInit() {
    console.log(GoogleMapComponent.mapsLoaded)
    this.init().then((res) => {
      console.log("Google Maps ready.")
      google.maps.event.trigger(this.map, 'resize');
    }, (err) => {
      console.log(err);

    });
  }

  private init(): Promise<any> {

    return new Promise((resolve, reject) => {

      this.loadSDK().then((res) => {

        this.initMap().then((res) => {
          resolve(true);
        }, (err) => {
          reject(err);
        });

      }, (err) => {

        reject(err);

      });

    });
  }
  private loadSDK(): Promise<any> {

    console.log("Loading Google Maps SDK");

    return new Promise((resolve, reject) => {

      if (!GoogleMapComponent.mapsLoaded) {

        Network.getStatus().then((status) => {

          if (status.connected) {

            this.injectSDK().then((res) => {
              resolve(true);
            }, (err) => {
              reject(err);
            });

          } else {

            if (this.networkHandler == null) {

              this.networkHandler = Network.addListener('networkStatusChange', (status) => {

                if (status.connected) {

                  this.networkHandler.remove();

                  this.init().then((res) => {
                    console.log("Google Maps ready.")
                  }, (err) => {
                    console.log(err);
                  });

                }

              });

            }

            reject('Not online');
          }

        }, (err) => {

          // NOTE: navigator.onLine temporarily required until Network plugin has web implementation
          if (navigator.onLine) {

            this.injectSDK().then((res) => {
              resolve(true);
            }, (err) => {
              reject(err);
            });

          } else {
            reject('Not online');
          }

        });

      } else {

        resolve(true);
      }

    });


  }
  private injectSDK(): Promise<any> {

    return new Promise((resolve, reject) => {

      window['mapInit'] = () => {
        GoogleMapComponent.mapsLoaded = true;
        resolve(true);
      }

      let script = this.renderer.createElement('script');
      script.id = 'googleMaps';
      script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
      this.renderer.appendChild(this._document.body, script);

    });

  }

  private initMap(): Promise<any> {

    let ref = this;
    /**
     * @constructor
     */
    var ClickEventHandler = function (map, origin) {
      this.origin = origin;
      this.map = map;
      this.placesService = new google.maps.places.PlacesService(map);
      this.infowindow = new google.maps.InfoWindow();
      this.infowindowContent = document.getElementById("infowindow-content");
      this.infowindow.setContent(this.infowindowContent);

      // Listen for clicks on the map.
      this.map.addListener("click", this.handleClick.bind(this));
    };

    ClickEventHandler.prototype.handleClick = async function (event) {
      if (event.placeId) {
        event.stop();
        this.getPlaceInformation(event.placeId);
      }
    };



    ClickEventHandler.prototype.getPlaceInformation = function (placeId) {
      var me = this;
      this.placesService.getDetails({ placeId: placeId }, function (place, status) {
        if (status === "OK") {
          me.infowindow.close();
          me.infowindow.addListener('domready', function () {
            document.getElementById("createNearby").addEventListener("click", function (e) {
              console.log(place.geometry.location);
              ref.onClickCreateGioGift.emit(place)
              e.stopPropagation();
            })
          });


          me.infowindow.setPosition(place.geometry.location);
          me.infowindowContent.children["place-icon"].src = place.icon;
          me.infowindowContent.children["place-name"].textContent = place.name;
          //me.infowindowContent.children["place-id"].textContent = place.place_id;
          me.infowindowContent.children["place-address"].textContent =
            place.formatted_address;

          me.infowindow.open(me.map);
        }
      });
    };


    return new Promise((resolve, reject) => {

      if (GoogleMapComponent.currentLocation == null) {
        this.geoLocationService.GetLocation().then((position) => {
          console.log(position);
          GoogleMapComponent.currentLocation = position

          let mapOptions = {
            center: new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude),
            zoom: 13,
            disableDefaultUI: true
          };
          this.map = new google.maps.Map(this.element.nativeElement, mapOptions);
          var clickHandler = new ClickEventHandler(this.map, new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude));
          resolve(true);

        }, (err) => {

          reject('Could not initialise map');

        });
      } else {

        let mapOptions = {
          center: new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude),
          zoom: 13,
          disableDefaultUI: true
        };
        this.map = new google.maps.Map(this.element.nativeElement, mapOptions);
        var clickHandler = new ClickEventHandler(this.map, new google.maps.LatLng(GoogleMapComponent.currentLocation.Latitude, GoogleMapComponent.currentLocation.Longitude));
        resolve(true);

      }

    });

  }


  public addMarker(markerInfo: MarkerInfo): void {
    let ref = this;
    let latLng = new google.maps.LatLng(markerInfo.Latitude, markerInfo.Longitude);

    var v = "Latitude_" + markerInfo.Latitude + "_Longitude_" + markerInfo.Longitude;
    console.log(v)

    if (Object.keys(ref.markers).indexOf(v) == -1) {
      let marker: any = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: latLng,
        icon: "https://nearable.app/map-marker-icons/" + markerInfo.Icon,
        title: markerInfo.Title,
        label: markerInfo.Label
      });
      ref.markers[v] = {};
      ref.markers[v].marker = marker;
      ref.markers[v].markerInfo = [markerInfo];
      marker.addListener('click', function () {

        var contentString = `<div id="info-content" class="info-content">`;

        ref.markers["Latitude_" + marker.position.lat() + "_Longitude_" + marker.position.lng()].markerInfo.forEach(element => {
          contentString += ` <h2 id="firstHeading" class="firstHeading" style="color:black">${element.Title}</h2><a class='viewLink' data-id="${element.AdditionalInfo.Id}">View Nearby</a>`
          console.log(element);
        });

        contentString += `</div>`;

        if (ref.infowindow) {
          ref.infowindow.close();
        }
        ref.infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        ref.infowindow.addListener('domready', function () {
          document.querySelectorAll(".viewLink").forEach(viewLink => {
            viewLink.addEventListener("click", function (e) {
              console.log(markerInfo);
              ref.markers["Latitude_" + marker.position.lat() + "_Longitude_"
                + marker.position.lng()].markerInfo.forEach(clickedMarkerInfo => {
                  if (clickedMarkerInfo.AdditionalInfo.Id == this.dataset.id)
                    ref.onClickViewFolder.emit(clickedMarkerInfo)
                });
              e.stopPropagation();
            });
          });
        });

        ref.infowindow.open(ref.map, marker);
      });

    } else {
      ref.markers[v].markerInfo.push(markerInfo)
    }

  }

  public async drawCircle(center: UserLocation, distanceInMeters) {
    await this.clearCircle()
    this.mapCircle = new google.maps.Circle({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.2,
      map: this.map,
      center: { lat: center.Latitude, lng: center.Longitude },
      radius: distanceInMeters
    });
  }

  public async clearCircle() {
    if (this.mapCircle) {
      this.mapCircle.setMap(null);
    }
  }
  public async clearMarker() {

    Object.keys(this.markers).forEach((markerKey) => {
      this.markers[markerKey].marker.setMap(null);
    });
    this.markers = {};
  }
}


