import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, LoadingController, Platform, AlertController } from '@ionic/angular';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth/auth.service';
import { FolderService } from '../services/folder/folder.service';
import { GeoLocationService } from '../services/geo-location/GeoLocation.service';
import { GoogleMapComponent } from '../component/google-map/google-map.component';
import { MarkerInfo, Folder } from '../interface/interface';
import { ToastService } from '../services/toast/toast.service';
import { CheckoutpagePage } from '../main/checkoutpage/checkoutpage.page';
import { ModalService } from 'src/app/services/modal/modal.service';
import { BusinessscreenPage } from '../main/businessscreen/businessscreen.page';
import { PasswordscreenPage } from '../main/passwordscreen/passwordscreen.page';
import { EmailscreenPage } from '../main/emailscreen/emailscreen.page';
import { StripeService } from '../services/stripe/stripe.service';
import { CheckoutPageViewModal } from '../ViewModal/CardInformation';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public userProfile: any = null;
  user: Observable<firebase.User>;
  users: any;
  public FolderUniqueId: string;
  private loader;
  public IsRegisteredUser = false;
  private userInfo: any
  @ViewChild(GoogleMapComponent, { static: true }) mapComponent: GoogleMapComponent;
  constructor(
    private navController: NavController,
    private loadingController: LoadingController,
    private gplus: GooglePlus,
    private platform: Platform,
    private afAuth: AngularFireAuth,
    private alertController: AlertController,
    private authService: AuthService,
    private folderService: FolderService,
    private geoLocationService: GeoLocationService,
    private toastService: ToastService,
    private modalService: ModalService,
    private stripeService: StripeService

  ) {
    this.user = this.afAuth.authState;

  }

  async ngOnInit() {
    console.log("login page arrived!");

  }
  async onClickCreateGioGift(place) {
    let ref = this;
    console.log(place)

    this.modalService.Present(BusinessscreenPage, (businessResponse) => {
      console.log(businessResponse);
      if (businessResponse.isCanceled) {
        return;
      }
      this.modalService.Present(PasswordscreenPage, (passwordDetail) => {
        console.log(passwordDetail);
        this.modalService.Present(CheckoutpagePage, (checkoutResult: CheckoutPageViewModal) => {
          console.log(checkoutResult);

          this.modalService.Present(EmailscreenPage, () => {


          })
        }, { PlaceInfo: place.name + " - " + place.formatted_address, PlaceId: place.place_id, PlaceName: place.name, PlaceAddress: place.formatted_address, Password: passwordDetail.Password, Latitude: place.geometry.location.lat(), Longitude: place.geometry.location.lng() })
      })
    }, { PlaceId: place.place_id, PlaceName: place.name, PlaceAddress: place.formatted_address })


  }
  async onClickViewFolder(markerInfo: MarkerInfo) {
    let folder = markerInfo.AdditionalInfo as Folder;

    if (folder.IsPrivateFence == "1") {
      if (this.userInfo.login) {
        if (this.userInfo.Id == folder.AuthorId) {
          this.navController.navigateForward('main/tab2', { queryParams: { folder: JSON.stringify(folder) } });
        } else {
          this.PrivateFolderLogin(folder);
        }

      } else {
        this.PrivateFolderLogin(folder);
      }

    } else {
      if (this.userInfo.login) {
        await this.folderService.AddSharedFolder(folder.SharedFolderId);
        this.navController.navigateForward('main/tab2', { queryParams: { folder: JSON.stringify(folder) } });
      } else {
        this.SearchByFolderId(folder.SharedFolderId)
      }
    }

  }

  async PrivateFolderLogin(folder: Folder) {
    let alert = await this.alertController.create({
      backdropDismiss: false,
      inputs: [
        {
          name: 'pCode',
          type: 'text',
          placeholder: 'Enter NearbyCode'
        }],
      header: 'Please enter folder name',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (args) => {
            console.log('Confirm Cancel', args);
          }
        }, {
          text: 'Join',
          handler: (args) => {
            if (args.pCode == folder.SharedFolderId) {
              if (this.userInfo.login) {
                this.folderService.AddSharedFolder(folder.SharedFolderId).then(() => {
                  this.navController.navigateForward('main/tab2', { queryParams: { folder: JSON.stringify(folder) } });
                });
              } else {
                this.SearchByFolderId(folder.SharedFolderId)
              }

            } else {
              this.toastService.presentToast("Invalid Code Entered !!")
              return false;
            }
            console.log('Confirm Ok', args);
          }
        }
      ]
    });
    await alert.present();
  }

  async ionViewWillEnter() {

    this.userInfo = await this.authService.GetUserInfoLocal();
    this.IsRegisteredUser = this.userInfo.login;

    this.mapComponent.clearMarker();
    this.geoLocationService.GetLocation().then((f) => {
      this.folderService.GetFoldersByLocation(f).then((folders) => {
        folders.forEach(folder => {
          this.mapComponent.addMarker({
            Latitude: folder.Latitude,
            Longitude: folder.Longitude,
            Title: folder.Name,
            Label: "",//folder.Name.length > 3 ? folder.Name.substr(0, 3) : folder.Name,
            Icon: folder.FolderIcon,
            AdditionalInfo: folder,
          });
        });
        console.log(folders);
      });
    })
  }

  async SearchByFolderId(folderId: string) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });

    loading.present();
    var folderInfo = await this.folderService.IsSharedFolderIdExists(folderId);

    if (folderInfo.Name) {
      this.authService.SaveUserLocalState({ ...folderInfo, login: false })
      this.navController.navigateForward('/main/tab2');
      this.FolderUniqueId = '';
    }
    else
      await this.presentAlert("Enter valid Folder ID")

    loading.dismiss();
  }

  goBackBtnClick() {
    this.navController.navigateForward('/main/camera-screen-new');
  }

  async login(profile) {
    console.log("login-profile: ", profile);

    const user = {
      gid: profile.id,
      name: profile.name,
      email: profile.email,
      photo: profile.picture,
      login_type: true,
      status: true,
      created_at: new Date(),
    }

    var userInfo = await this.authService.SaveUser({ Email: profile.email, Name: profile.name, Image: user.photo });
    this.authService.SaveUserLocalState({ ...userInfo, login: true });
    this.navController.navigateForward('/main/camera-screen-new');
    this.loader.dismiss();

  }

  async GoogleLogin() {
    this.loader = await this.loadingController.create({
      message: 'Sign you in...',
    });
    this.loader.present();
    if (this.platform.is('cordova')) {
      await this.NativeGoogleLogin();
    } else {
      await this.WebGoogleLogin();
    }
  }

  async NativeGoogleLogin() {
    this.gplus.login({
      'scopes': '', // optional - space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
      'webClientId': 'com.googleusercontent.apps.947242011312-0gb1319gcfn51t86kt4jfja0ltr5kev8', // optional - clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
      'offline': true, // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode, which can be used to grant offline access to a non-Google server
    }).then(user => {
      this.login(user);
    }, err => {
      console.log("NativeGoogleLogin-err: ", err);
      if (!this.platform.is('cordova')) {
        this.presentAlert('Cordova is not available on desktop. Please try this in a real device or in an emulator.');
      }
      this.loader.dismiss();
    })
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentLoading(loading) {
    return await loading.present();
  }

  async WebGoogleLogin(): Promise<void> {
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const credential = await this.afAuth.auth.signInWithPopup(provider);
      const profile: any = credential.additionalUserInfo.profile;
      console.log("WebGoogleLogin-success! credentical: ", profile);
      this.login(profile);
    } catch (err) {
      console.log("WecGoogleLogin-err: ", err);
      this.loader.dismiss();
    }
  }

  SignOut() {
    this.afAuth.auth.signOut();
    if (this.platform.is('cordova')) {
      this.gplus.logout();
    }
  }

  async AppleLogin(): Promise<any> {
    console.log("apple login function");
  }

  async OpenPaymentcheckoutPage() {
    let ref = this;
    await ref.modalService.Present(CheckoutpagePage, async () => {

    });
  }

}
